package com.example.pedronsouza.dribbleclient.models.entities;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pedrosouza on 8/18/15.
 */

public class Player implements Parcelable {
    @Expose
    private int id;
    @Expose
    @SerializedName("avatar_url")
    private String avatarUrl;
    @Expose
    private String username;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.avatarUrl);
        dest.writeString(this.username);
    }

    public Player() {
    }

    protected Player(android.os.Parcel in) {
        this.id = in.readInt();
        this.avatarUrl = in.readString();
        this.username = in.readString();
    }

    public static final Parcelable.Creator<Player> CREATOR = new Parcelable.Creator<Player>() {
        public Player createFromParcel(android.os.Parcel source) {
            return new Player(source);
        }

        public Player[] newArray(int size) {
            return new Player[size];
        }
    };
}
