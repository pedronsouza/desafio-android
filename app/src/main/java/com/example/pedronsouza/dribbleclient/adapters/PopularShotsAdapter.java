package com.example.pedronsouza.dribbleclient.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.pedronsouza.dribbleclient.R;
import com.example.pedronsouza.dribbleclient.activities.ShotDetailsActivity;
import com.example.pedronsouza.dribbleclient.models.entities.Shot;
import com.example.pedronsouza.dribbleclient.utils.Finder;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.List;

/**
 * Created by zeroum on 8/17/15.
 */
public class PopularShotsAdapter extends RecyclerView.Adapter<PopularShotsAdapter.ViewHolder> {

    private final Context mContext;
    private List<Shot> shots;

    public PopularShotsAdapter(Context context, List<Shot> shots) {
        this.shots = shots;
        mContext = context;
    }

    public List<Shot> getShots() {
        return shots;
    }

    public void setShots(List<Shot> shots) {
        this.shots = shots;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.popular_shot_item, parent, false);
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Shot shot = shots.get(position);
        holder.title.setText(shot.getTitle());
        holder.viewsCount.setText(String.valueOf(shot.getViewsCount()));
        holder.setImage(shot.getImageUrl());
        holder.setOnClickListener(shotDetailsClickListener);
        holder.image.setTag(position);
    }

    @Override
    public int getItemCount() {
        return shots.size();
    }

    public void addShots(List<Shot> shots) {
        this.shots.addAll(shots);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView viewsCount;
        private SimpleDraweeView image;

        public ViewHolder(View itemView) {
            super(itemView);
            title = Finder.findViewById(itemView, R.id.popular_shot_title);
            viewsCount = Finder.findViewById(itemView, R.id.popular_shot_views_count);
            image = Finder.findViewById(itemView, R.id.popular_shot_image);
        }

        public void setOnClickListener(View.OnClickListener l) {
            image.setOnClickListener(l);
        }

        public void setImage(String imageUrl) {
            ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(imageUrl))
                    .setProgressiveRenderingEnabled(true)
                    .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                    .build();
            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setOldController(image.getController())
                    .setAutoPlayAnimations(true)
                    .setImageRequest(request)
                    .build();

            image.setController(controller);
        }
    }

    private final View.OnClickListener shotDetailsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Shot shot = shots.get((Integer) v.getTag());
            Intent shotDetails = new Intent(mContext, ShotDetailsActivity.class);
            shotDetails.putExtra(ShotDetailsActivity.SHOT_KEY, shot);
            mContext.startActivity(shotDetails);
        }
    };
}
