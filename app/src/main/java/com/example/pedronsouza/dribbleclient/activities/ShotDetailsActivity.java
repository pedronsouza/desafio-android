package com.example.pedronsouza.dribbleclient.activities;

import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.pedronsouza.dribbleclient.R;
import com.example.pedronsouza.dribbleclient.models.entities.Shot;
import com.example.pedronsouza.dribbleclient.utils.Finder;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

public class ShotDetailsActivity extends BaseActivity {
    public final static String SHOT_KEY = "ShotDetailsActivity.SHOT_KEY";
    private Shot mShot;
    private TextView mTitle;
    private TextView mViewsCount;
    private SimpleDraweeView mImage;
    private SimpleDraweeView mUserThumb;
    private TextView mUserName;
    private TextView mDescription;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_shot_details;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mShot = getIntent().getExtras().getParcelable(SHOT_KEY);

        if (mToolbar != null) {
            mToolbar.setTitle(mShot.getTitle());
            setBackButton();
        }
        setupInterface();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            super.onBackPressed();

        return false;
    }

    private void setupInterface() {
        mTitle = Finder.findViewById(this, R.id.popular_shot_title);
        mViewsCount = Finder.findViewById(this, R.id.popular_shot_views_count);
        mImage = Finder.findViewById(this, R.id.popular_shot_image);
        mUserThumb = Finder.findViewById(this, R.id.shot_details_user_thumb);
        mUserName = Finder.findViewById(this, R.id.shot_details_username);
        mDescription = Finder.findViewById(this, R.id.shot_details_description);

        mTitle.setText(mShot.getTitle());
        mViewsCount.setText(String.valueOf(mShot.getViewsCount()));
        mUserName.setText(mShot.getPlayer().getUsername());
        if (mShot.getDescription() != null)
            mDescription.setText(Html.fromHtml(mShot.getDescription()).toString());

        setDraweeController(mImage, mShot.getImageUrl());
        setDraweeController(mUserThumb, mShot.getPlayer().getAvatarUrl());

    }

    private void setDraweeController(SimpleDraweeView image, String url) {
        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(url))
                .setProgressiveRenderingEnabled(true)
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .build();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setOldController(image.getController())
                .setImageRequest(request)
                .setAutoPlayAnimations(true)
                .build();

        image.setController(controller);
    }
}
