package com.example.pedronsouza.dribbleclient.models.entities;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zeroum on 8/17/15.
 */
public class Shot implements Parcelable {
    @Expose
    private int id;
    @Expose
    @SerializedName("views_count")
    private int viewsCount;
    @Expose
    private String title;
    @Expose
    @SerializedName("image_url")
    private String imageUrl;
    @Expose
    private String description;
    @Expose
    private Player player;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(int viewsCount) {
        this.viewsCount = viewsCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.viewsCount);
        dest.writeString(this.title);
        dest.writeString(this.imageUrl);
        dest.writeString(this.description);
        dest.writeParcelable(this.player, 0);
    }

    public Shot() {
    }

    protected Shot(android.os.Parcel in) {
        this.id = in.readInt();
        this.viewsCount = in.readInt();
        this.title = in.readString();
        this.imageUrl = in.readString();
        this.description = in.readString();
        this.player = in.readParcelable(Player.class.getClassLoader());
    }

    public static final Parcelable.Creator<Shot> CREATOR = new Parcelable.Creator<Shot>() {
        public Shot createFromParcel(android.os.Parcel source) {
            return new Shot(source);
        }

        public Shot[] newArray(int size) {
            return new Shot[size];
        }
    };
}
