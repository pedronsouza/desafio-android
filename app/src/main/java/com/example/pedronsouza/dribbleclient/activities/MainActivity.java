package com.example.pedronsouza.dribbleclient.activities;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.pedronsouza.dribbleclient.R;
import com.example.pedronsouza.dribbleclient.adapters.PopularShotsAdapter;
import com.example.pedronsouza.dribbleclient.models.DribbleApiClient;
import com.example.pedronsouza.dribbleclient.models.entities.PaginatedPopularShots;
import com.example.pedronsouza.dribbleclient.models.entities.Shot;
import com.example.pedronsouza.dribbleclient.support.ui.EndlessRecyclerView;
import com.example.pedronsouza.dribbleclient.utils.Finder;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends BaseActivity {
    private static final String INSTANCE_SHOTS = "MainActivity.INSTANCE_SHOTS";
    private static final String INSTANCE_PAGE = "MainActivity.INSTANCE_PAGE";
    private static final String INSTANCE_PAGINATED_SHOTS = "MainActivity.INSTANCE_PAGINATED_SHOTS";
    private PaginatedPopularShots paginatedPopularShots;
    private EndlessRecyclerView mRecyclerView;
    private int page = 1;
    private PopularShotsAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressBar mProgressBar;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRecyclerView = Finder.findViewById(this, R.id.activity_main_recyclerview);
        mSwipeRefreshLayout = Finder.findViewById(this, R.id.swipeRefreshLayout);
        mProgressBar = Finder.findViewById(this, R.id.progress);
        mProgressBar.setIndeterminate(true);

        mSwipeRefreshLayout.setOnRefreshListener(refreshListener);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setOnLoadMore(onLoadMore);

        if (savedInstanceState != null) {
            List<Shot> shots = savedInstanceState.getParcelableArrayList(INSTANCE_SHOTS);
            page = savedInstanceState.getInt(INSTANCE_PAGE);
            paginatedPopularShots = savedInstanceState.getParcelable(INSTANCE_PAGINATED_SHOTS);

            mAdapter = new PopularShotsAdapter(this, shots);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            sendRequest();
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(INSTANCE_SHOTS, (ArrayList<Shot>) mAdapter.getShots());
        outState.putInt(INSTANCE_PAGE, page);
        outState.putParcelable(INSTANCE_PAGINATED_SHOTS, paginatedPopularShots);
        super.onSaveInstanceState(outState);
    }

    private void setupRecyclerView() {
        if (mAdapter == null) {
            mAdapter = new PopularShotsAdapter(this, paginatedPopularShots.getShots());
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.addShots(paginatedPopularShots.getShots());
            mAdapter.notifyDataSetChanged();
        }
    }

    private final Callback<PaginatedPopularShots> popularShotsCallback = new Callback<PaginatedPopularShots>() {
        @Override
        public void success(PaginatedPopularShots paginatedPopularShots, Response response) {
            mProgressBar.setVisibility(View.GONE);
            if (mSwipeRefreshLayout.isRefreshing())
                mSwipeRefreshLayout.setRefreshing(false);

            MainActivity.this.paginatedPopularShots = paginatedPopularShots;
            setupRecyclerView();
        }

        @Override
        public void failure(RetrofitError error) {
            mProgressBar.setVisibility(View.GONE);
            Log.d("HTTP Error", error.getMessage());
        }
    };

    private void sendRequest() {
        mProgressBar.setVisibility(View.VISIBLE);
        DribbleApiClient.getInstance().getPopularShots(page, popularShotsCallback);
    }

    private final EndlessRecyclerView.OnLoadMore onLoadMore = new EndlessRecyclerView.OnLoadMore() {
        @Override
        public void loadMore(int currentPage) {
            if (paginatedPopularShots.getPages() > page) {
                page++;
                sendRequest();
            }
        }
    };

    private final SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            page = 1;
            mAdapter = null;
            sendRequest();
        }
    };
}
