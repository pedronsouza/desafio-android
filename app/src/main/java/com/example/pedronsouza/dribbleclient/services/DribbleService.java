package com.example.pedronsouza.dribbleclient.services;

import com.example.pedronsouza.dribbleclient.models.entities.PaginatedPopularShots;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by zeroum on 8/17/15.
 */
public interface DribbleService {
    @GET("/shots/popular")
    void paginatedShots(@Query("page") int page, Callback<PaginatedPopularShots> callback);
}
