package com.example.pedronsouza.dribbleclient;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by zeroum on 8/17/15.
 */
public class DribbleClientApplication extends Application {
    private static DribbleClientApplication mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        mContext = this;
    }

    public static DribbleClientApplication application() {
        return mContext;
    }
}
