package com.example.pedronsouza.dribbleclient.models.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zeroum on 8/17/15.
 */

public class PaginatedPopularShots implements Parcelable {
    @Expose
    private String page;
    @Expose
    @SerializedName("per_page")
    private int perPage;
    @Expose
    private int pages;
    @Expose
    private List<Shot> shots;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<Shot> getShots() {
        return shots;
    }

    public void setShots(List<Shot> shots) {
        this.shots = shots;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.page);
        dest.writeInt(this.perPage);
        dest.writeInt(this.pages);
        dest.writeList(this.shots);
    }

    public PaginatedPopularShots() {
    }

    protected PaginatedPopularShots(Parcel in) {
        this.page = in.readString();
        this.perPage = in.readInt();
        this.pages = in.readInt();
        this.shots = new ArrayList<Shot>();
        in.readList(this.shots, List.class.getClassLoader());
    }

    public static final Parcelable.Creator<PaginatedPopularShots> CREATOR = new Parcelable.Creator<PaginatedPopularShots>() {
        public PaginatedPopularShots createFromParcel(Parcel source) {
            return new PaginatedPopularShots(source);
        }

        public PaginatedPopularShots[] newArray(int size) {
            return new PaginatedPopularShots[size];
        }
    };
}
