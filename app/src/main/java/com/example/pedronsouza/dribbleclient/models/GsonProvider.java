package com.example.pedronsouza.dribbleclient.models;

import com.example.pedronsouza.dribbleclient.serializers.DateSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

/**
 * Created by zeroum on 8/17/15.
 */
public class GsonProvider {
    private static Gson gson = new GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .registerTypeAdapter(Date.class, new DateSerializer())
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            .create();

    private GsonProvider() {

    }

    public static Gson getGson() {
        return gson;
    }
}
