package com.example.pedronsouza.dribbleclient.utils;

import android.app.Activity;
import android.view.View;

/**
 * Created by zeroum on 4/13/15.
 */
public class Finder {

    public static <T> T findViewById(Activity context, int id) {
        return (T)context.findViewById(id);
    }

    public static <T> T findViewById(View view, int id) {
        return (T)view.findViewById(id);
    }

}