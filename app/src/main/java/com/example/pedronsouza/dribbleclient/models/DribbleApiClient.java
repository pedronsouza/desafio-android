package com.example.pedronsouza.dribbleclient.models;

import com.example.pedronsouza.dribbleclient.models.entities.PaginatedPopularShots;
import com.example.pedronsouza.dribbleclient.services.DribbleService;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by zeroum on 8/17/15.
 */
public class DribbleApiClient {
    private static final String ENDPOINT = "https://api.dribbble.com/";
    private final RestAdapter restAdapter;


    public DribbleApiClient() {
        this.restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setConverter(new GsonConverter(GsonProvider.getGson()))
                .build();
    }

    public static DribbleApiClient getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void getPopularShots(int page, Callback<PaginatedPopularShots> paginatedPopularShotsCallback) {
        DribbleService service = restAdapter.create(DribbleService.class);
        service.paginatedShots(page, paginatedPopularShotsCallback);
    }

    private static class SingletonHolder {
        private static final DribbleApiClient INSTANCE = new DribbleApiClient();
    }
}
